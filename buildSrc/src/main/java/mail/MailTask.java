package mail;

import org.gradle.api.*;
import org.gradle.api.tasks.*;

public class MailTask extends DefaultTask {
    String content;
    String to;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @TaskAction
    public void sendMail() throws Exception {
//        getLooper.lifecycle(" \\ отправлено к " + to);
        System.out.println(" \\ отправлено к " + to);
    }
}