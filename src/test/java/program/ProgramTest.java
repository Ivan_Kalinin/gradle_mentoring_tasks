package program;

import org.junit.*;

public class ProgramTest {

    @Test
    public void testMessage() {
        Program program = new Program();
        String message = program.getMessage();
        System.out.println("message " + message);
        Assert.assertEquals("message", message);
    }
}